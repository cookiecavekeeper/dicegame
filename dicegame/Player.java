package dicegame;                                                                             //Package

import java.util.Scanner;                                                                     //Scanner importieren


public class Player {                                                                         //Start Klassenbody
    
    Scanner scan = new Scanner(System.in);                                                    //Neues Scanner Objekt erstellen
    
    public String name = "";                                                                  //String-Variable "name" erstellen und initiieren
    
    public int points = 0;                                                                    //int "points" erstellen und initiieren
    
    public Player(String name) {                                                              //Konstruktor für das Erstellen eines "Player"-Objekts
        this.name = name;                                                                     //Argument "name" des Konstruktors wird zum Wert vom String "name"
    }
    
    public void playRound() {                                                                 //Methode "PlayRound" erstellen
        int temp = 0;                                                                         //int "temp" mit Wert 0 erstellen
        for(int i = 1; i <= 5; i++) {                                                         //for-Schleife für 5 Durchgänge starten
            System.out.println(this.name + " willst du das " + i + ". mal würfeln?(j/n)");    //Spricht spieler an und fragt ob gewürfelt werden soll
            String answer = scan.next();                                                      //Nächste Eingabe eines Strings wird zum Wert des soeben initiierten Strings "answer" 
            if(answer.equals("n")) {                                                          //wenn-Abfrage "answer="n"
                break;                                                                        //Der Zug wird beendet
            }
            int temp2 = Dice.rollDice();                                                      //Ansonsten wird mit der Dice.rollDice() methode gewürfelt
            if(temp2 == 1) {                                                                  //falls das Resultat 1 ist ..
                System.out.println(Dice.getImg(temp2));
                System.out.println("Du hast eine 1 Gewürfelt... Viel glück nächstes Mal =(");
                return;                                                                       //beommt der Spieler keine Punkt und die Funktion wird beendet
            } else {
                System.out.println(Dice.getImg(temp2));                                       //Ansonsten wird das ASCII bild des Würfels ausgegeben mi der Dice.getImg() methode
            }
            temp += temp2;                                                                    //Die würfelaugen werden zur Temp Punktzahl addiert
        }
        points += temp;                                                                       //Am ende der Runde werden die Temp punkte zum Total hinzugefügt
        System.out.println(name + " hat " + temp + " Punkte bekommen diese Runde!");          // Und der Punktestand ausgegeben
        System.out.println(name + " hat jetzt " + points + " Punkte!");
    }
}
