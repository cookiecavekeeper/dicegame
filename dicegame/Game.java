package dicegame;                                                               //Das package dicegame wird benutzt

import java.util.Scanner;                                                       //Der Scanner wird importiert

public class Game {
    
    Scanner scan = new Scanner(System.in);                                      //Der scanner wird als attribut erstellt
    
    public final int POINT_GOAL = 100;                                          //Das Punkteziel wird als final int deklariert
    
    Player[] players;                                                           //Ein Arary vom Typ Player wird deklariert
    
    int roundCount = 0;                                                         //Die Rundenzahl vom typ int wird mit dem Wert 0 deklariert
    
    boolean keepPlaying = true;                                                 //Die boolishce variable keepPlaying wird mit wert true deklariert
    
    public Game(int playerAmount) {                                             //Konstruktor der Klasse mit einem Argument vom typ int
        players = new Player[playerAmount];                                     //Die Arary varibale wird mit der Anzahl Spieler(Argument) initialisiert
        
        for(int i = 0; i < playerAmount; i++) {                                 //Ein for-loop mit der Läng der anzahl Spieler
            System.out.println("Bitte Namen für " + (i+1) + ". Spieler eingeben:"); //Für den n-ten spieler wird der name angefordert
            String name = scan.next();                                          //String aus Konsole wird in variable eingelesen
            players[i] = new Player(name);                                      //ein neuse Player Objekt mit namen als parameter in arary
        }
        
    }
    
    public void play() {                                                        //play methode erstellt
        while(keepPlaying) {                                                    //while loop mit boolean keepPlaying als bedingung
            playRound();                                                        //playRound() Methode wird aufgerufen
        }
        
        showScores();                                                           //Wennd er Loop beendet wird, wird showScores() aufgerufen
    }
    
    public void playRound() {                                                   //Methode wird erstellt
        System.out.println("Runde " + ++roundCount + " wird gespielt!");        //roundCount wird erhöht und ausgegeben
        for(Player player : players) {                                          //for / each loop mit players array
            player.playRound();                                                 //die playRound() jedes objekts wird aufgerufen
            
            if(player.points > POINT_GOAL) {                                    //Wenn ein spieler genügend punkte hat..
                keepPlaying = false;                                            //wird variable auf false gesetzt
            }
        }
    }
    
    public void showScores() {                                                  //Methode wir erstellt
        Player[] scoreList = players;                                           //das players array wird als scoreList geklont
        int rank = 1;                                                           //int rank wird mit wert 1 erstellt
        
        while(scoreList.length > 0) {                                           //while Schlaufe solange bis scoreList leer ist
            Player top = scoreList[0];                                          //der ertse Spieler wird als top gespeichert
            
            for(Player player : scoreList) {                                    //for each durch player array
                if(player.points > top.points) {                                //falls der spieler besser als top ist...
                    top = player;                                               //wir er top
                }
            }
            System.out.println("Platz " + rank++ + " : " + top.name + " mit " + top.points + " Punkten"); //top's Punkte, Rang und Name werden ausgegeben
            scoreList = remArray(scoreList, top);                               //die funktion remArary wird mit den parametern scorelist und top aufgerufen
                                                                                //das Resultat wird in scoreList gespeichert
        }
        
        System.out.println("Danke fürs spielen.");                              //Bedankung wird ausgegeben
    }
    
    public Player[] remArray(Player[] input, Player toRemove) {                 //remArray wird erstellt Parameter: input(ein Player Array), toRemove (ein Player objekt)
        Player[] output = new Player[input.length - 1];                         //player array wird erzeugt, eins kürzer als input
        
        for(int i = 0, o = 0; i < input.length; i++) {                          //for loop mit input.lenght als Länge
            if(input[i] != toRemove) {                                          //jedes element das nicht toRemove ist...
                output[o++] = input [i];                                        //wird ins output array gespeichert
            }
        }
        
        return output;                                                          //output wird returniert
    }
}
