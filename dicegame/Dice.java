package dicegame;   //Package


public class Dice {     //Start Klassenbody "Dice
    
    public static final String ONE =   "/-------\\\n" +     //String mit ASCII-Art Würfel mit der Augenzahl 1
                                "|       |\n" +
                                "|   O   |\n" +
                                "|       |\n" +
                                "\\-------/";
    
    public static final String TWO =   "/-------\\\n" +     //String mit ASCII-Art Würfel mit der Augenzahl 2
                                "|     O |\n" +
                                "|       |\n" +
                                "| O     |\n" +
                                "\\-------/";
    
    public static final String THREE = "/-------\\\n" +     //String mit ASCII-Art Würfel mit der Augenzahl 3
                                "|     O |\n" +
                                "|   O   |\n" +
                                "| O     |\n" +
                                "\\-------/";
    
    public static final String FOUR =  "/-------\\\n" +     //String mit ASCII-Art Würfel mit der Augenzahl 4
                                "| O   O |\n" +
                                "|       |\n" +
                                "| O   O |\n" +
                                "\\-------/";
    
    public static final String FIVE =  "/-------\\\n" +     //String mit ASCII-Art Würfel mit der Augenzahl 5
                                "| O   O |\n" +
                                "|   O   |\n" +
                                "| O   O |\n" +
                                "\\-------/";
    
    public static final String SIX =   "/-------\\\n" +     //final String mit ASCII-Art Würfel mit der Augenzahl 6
                                "| O   O |\n" +
                                "| O   O |\n" +
                                "| O   O |\n" +
                                "\\-------/";
    
    public static String getImg(int number) {           //Public string "getImg" wird erstellt und ein int-Argument "number" angegeben
        switch(number) {        //switch Abfrage mit dem Argument "number"
            case 1:
                return ONE; //wenn number = 1 wird String "ONE" ausgegeben
            case 2:
                return TWO; //wenn number = 2 wird String "TWO" ausgegeben
            case 3:
                return THREE;   //wenn number = 3 wird String "THREE" ausgegeben
            case 4:
                return FOUR;    //wenn number = 4 wird String "FOUR" ausgegeben
            case 5:
                return FIVE;   //wenn number = 5 wird String "FIVE" ausgegeben
            case 6:
                return SIX;     //wenn number = 6 wird String "SI" ausgegeben
            default:
                return "";      //in jedem anderen Fall wird nichts ausgegeben 
        }
    }
    
    public static int rollDice() {  //Methode "rollDice" erstellen
        System.out.println();   //leere Zeile ausgeben
        return (int)(Math.random() * 6 + 1);    //Simulation des Würfelns mit Hilfe der Math.random() funktion
    }
    
}
