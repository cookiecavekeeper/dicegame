package dicegame;                                                               //Das dicegame package wird benutzt

import java.util.Scanner;                                                       //Der Scanenr wird importiert


public class DiceGame {

    public static Scanner scan = new Scanner(System.in);                        //Scanner wir statisch Initialisiert
    
    public static void main(String[] args) {
        System.out.println("Willkommen bei unserem Spiel!");                    //Begrüssung wird in Konsole ausgegeben
        System.out.println("Wie viele Spieler werden spielen?");                //nachfrage wir ausgegeben
        
        Game game = new Game(scan.nextInt());                                   //Ein neuse Game objekt wird erzeugt und die zahl aus der Konsole als parameter mitgegeben
        
        game.play();                                                            //Die .play() Methode des Game Objekts wird aufgerufen
    }
    
}
